/*
**Proyecto: pryOpAritmeticas
**Lugar: Iguala, Guerrero
**Programador: Oscar
**Fecha: 18/09/2018
**Descripción: Manejo de Condicionales if, operadores aritmeticos,
**             y variables de  tipo entero.
**Nivel: Basico
*/

#include <iostream>

using namespace std;

int main()
{
    int nOpcion = 0;
    int npNumero = 0;
    int nsNumero = 0;
    int nResultado = 0;

    cout << "Operaciones Aritmeticas con Dos Numeros Enteros" << endl;
    cout << "===============================================" << endl;
    cout << "Digite un Numero: ";
        cin >> npNumero;
    cout << "Digite otro Numero: ";
        cin >> nsNumero;
    cout <<endl;

    cout << "1) Sumar" << endl;
    cout << "2) Restar" << endl;
    cout << "3) Multiplicar" << endl;
    cout << "4) Dividir" << endl;
    cout << "5) Salir" << endl;
    cout << "Digite una Opcion: ";
        cin >> nOpcion;

    if(nOpcion == 1)
    {
      nResultado = npNumero + nsNumero;
      cout << "La suma es:" << nResultado << endl;
    }

    if(nOpcion == 2)
    {
      nResultado = npNumero - nsNumero;
      cout << "La resta es:" << nResultado << endl;
    }

    if(nOpcion == 3)
    {
      nResultado = npNumero * nsNumero;
      cout << "El Producto es:" << nResultado << endl;
    }

    if(nOpcion == 4)
    {
      nResultado = npNumero * nsNumero;
      cout << "El Cociente es:" << nResultado << endl;
    }

    return 0;
}
